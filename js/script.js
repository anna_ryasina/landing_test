/**
 * Created by user on 16.06.2017.
 */
$(document).ready(function () {
    $('.single-item').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: true,
                    slidesToShow: 1
                }
            }
        ]
    });
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('#contact-form').on('submit',function () {
        $('input,textarea',$(this)).each(function () {
            if($(this).val()==''){
                alert('Enter all field');
                return false;
            }
        });
        if(! validateEmail($('input[type="email"]',$(this)).val())){
            alert('No valid email');
            return false;
        }

    });
    $('#menu-toggle').on('click',function () {
        $(this).toggleClass('open');
        $('ul.nav').toggleClass( "active", 1000, "easeOutSine");
        return false;
    });
});